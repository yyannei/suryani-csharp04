﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Suryani_Csharpday5
{
    public class Monster
    {
        public int MonsterHp { get; set; }

        public Monster(int hp)
        {
            MonsterHp = hp;
        }
    }
        
}
