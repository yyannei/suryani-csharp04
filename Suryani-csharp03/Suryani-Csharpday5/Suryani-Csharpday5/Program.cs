﻿using System;


namespace Suryani_Csharpday5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to pongpong battle field.");

            Console.Write("Input your name :");
            string yourName = Console.ReadLine();
            Console.WriteLine("Hello"+" "+ yourName + ".");
            
            Weapon axe = new Weapon("Axe", 4000);
            Weapon sword = new Weapon("Sword", 3000);
            Weapon bomb = new Weapon("Bomb", 5000);

            Weapon[] arrayOfWeapon = {
                axe,
                sword,
                bomb
            };

            for (var i = 0; i < arrayOfWeapon.Length; i++)
            {
                Console.WriteLine($"{i + 1}, {arrayOfWeapon[i].Name}");
            }

            Console.Write("Input the number of weapon that you choose : ");
            int Weapon = 0;
            bool fail = true;
            while (fail)
            { 
                fail = int.TryParse(Console.ReadLine(), out Weapon) == false;
                Console.Write("Please input with number:");
            }
            Console.WriteLine("Hello"+" "+ yourName + ", " + "you have chosen" + " " + arrayOfWeapon[Weapon-1].Name + " " + "as your weapon "+ "with deal damage " + arrayOfWeapon[Weapon-1].Damage + "hp.");

            var selectedWeapon = arrayOfWeapon[Weapon - 1];
            Monster Monster = new Monster(10000);
            int counter = 0;
            while (Monster.MonsterHp > 0)
            {
                Monster.MonsterHp = (Monster.MonsterHp - selectedWeapon.Damage);
                counter = counter + 1;
            }
            Console.WriteLine("You have defeated a monster with 10000hp using " + arrayOfWeapon[Weapon - 1].Name + " " + "in " + counter + " " + "turn");
        }

    }
}
